package sdc.office.inventory

import org.scalatest.flatspec.AnyFlatSpecLike
import sdc.kitchen.recipe.Ingredient.Implicits._
import sdc.kitchen.recipe.Ingredient._
import sdc.office.OfficeTestHelper.assertRequiredInventory

class Menu3 extends sdc.office.menu.Menu3 with AnyFlatSpecLike {

	behavior of "Menu3 Inventory"
	
	it should "generate an inventory update" in {
		assertRequiredInventory(
			items = recipes
		)(
			flour = Some(Flour(4.56)) + Some(Flour(4.56)),
			water = Some(Water(1.95)) + Some(Water(1.95)),
			salt = Some(Salt(1.3)) + Some(Salt(1.3)),
			oil = Some(Oil(4.0)) + Some(Oil(4.0)),
			sugar = Some(Sugar(0.5999)) + Some(Sugar(0.5999)),
			vanilla = Some(Vanilla(1.0)),
			yeast = Some(Yeast(1.44)) + Some(Yeast(1.45)),
			bakingPowder = Some(BakingPowder(1.0)),
			butter = Some(Butter(2)),
			eggs = Some(Eggs(4)),
			milk = Some(Milk(1.25))
		)
	}

}