package sdc.office.menu

import sdc.kitchen.Kitchen
import sdc.kitchen.recipe.Ingredient._
import sdc.kitchen.recipe.Recipe

trait Menu2 {

	val recipes: Seq[Recipe] = Seq(
		Kitchen.bread(Some(Yeast(1.44)), Some(Water(1.95)), Some(Flour(4.6)), Some(Oil(4.0)), Some(Salt(1.33)), Some(Sugar(0.41)))
	)

}