package sdc.kitchen.recipe

/** `Ingredient`s are used to make `Recipe`s. */
object Ingredient {
	case class Yeast        (Tbsp:   Double)
	case class Water        (cups:   Double)
	case class Flour        (cups:   Double)
	case class Oil          (Tbsp:   Double)
	case class Salt         (tsp:    Double)
	case class Sugar        (cups:   Double)
	case class Butter       (sticks: Int)
	case class BakingPowder (Tbsp:   Double)
	case class Eggs         (count:  Int)
	case class Milk         (cups:   Double)
	case class Vanilla      (Tbsp:   Double)

	object Implicits {

		private def ifNotBothEmpty[T](a: Option[T], b: Option[T])(f: (Option[T], Option[T]) => Option[T]): Option[T] =
			if (a.isEmpty && b.isEmpty) None else f(a, b)

		implicit class OptionYeastOps(maybe: Option[Yeast]) {
			def maybeValue: Option[Double] = maybe.map(_.Tbsp)

			def +(other: Option[Yeast]): Option[Yeast] = {
				ifNotBothEmpty(maybe, other) { (a, b) => 
					Some(Yeast(a.maybeValue + b.maybeValue))
				}
			}
		}

		implicit class OptionWaterOps(maybe: Option[Water]) {
			def maybeValue: Option[Double] = maybe.map(_.cups)

			def +(other: Option[Water]): Option[Water] = {
				ifNotBothEmpty(maybe, other) { (a, b) =>
					Some(Water(a.maybeValue + b.maybeValue))
				}
			}
		}

		implicit class OptionFlourOps(maybe: Option[Flour]) {
			def maybeValue: Option[Double] = maybe.map(_.cups)

			def +(other: Option[Flour]): Option[Flour] = {
				ifNotBothEmpty(maybe, other) { (a, b) =>
					Some(Flour(a.maybeValue + b.maybeValue))
				}
			}
		}

		implicit class OptionOilOps(maybe: Option[Oil]) {
			def maybeValue: Option[Double] = maybe.map(_.Tbsp)

			def +(other: Option[Oil]): Option[Oil] = {
				ifNotBothEmpty(maybe, other) { (a, b) =>
					Some(Oil(a.maybeValue + b.maybeValue))
				}
			}
		}

		implicit class OptionSaltOps(maybe: Option[Salt]) {
			def maybeValue: Option[Double] = maybe.map(_.tsp)

			def +(other: Option[Salt]): Option[Salt] = {
				ifNotBothEmpty(maybe, other) { (a, b) =>
					Some(Salt(a.maybeValue + b.maybeValue))
				}
			}
		}

		implicit class OptionSugarOps(maybe: Option[Sugar]) {
			def maybeValue: Option[Double] = maybe.map(_.cups)

			def +(other: Option[Sugar]): Option[Sugar] = {
				ifNotBothEmpty(maybe, other) { (a, b) =>
					Some(Sugar(a.maybeValue + b.maybeValue))
				}
			}
		}

		implicit class OptionButterOps(maybe: Option[Butter]) {
			def maybeValue: Option[Int] = maybe.map(_.sticks)

			def +(other: Option[Butter]): Option[Butter] = {
				ifNotBothEmpty(maybe, other) { (a, b) =>
					Some(Butter(a.maybeValue + b.maybeValue))
				}
			}
		}

		implicit class OptionBakingPowderOps(maybe: Option[BakingPowder]) {
			def maybeValue: Option[Double] = maybe.map(_.Tbsp)

			def +(other: Option[BakingPowder]): Option[BakingPowder] = {
				ifNotBothEmpty(maybe, other) { (a, b) =>
					Some(BakingPowder(a.maybeValue + b.maybeValue))
				}
			}
		}

		implicit class OptionEggsOps(maybe: Option[Eggs]) {
			def maybeValue: Option[Int] = maybe.map(_.count)

			def +(other: Option[Eggs]): Option[Eggs] = {
				ifNotBothEmpty(maybe, other) { (a, b) =>
					Some(Eggs(a.maybeValue + b.maybeValue))
				}
			}
		}

		implicit class OptionMilkOps(maybe: Option[Milk]) {
			def maybeValue: Option[Double] = maybe.map(_.cups)

			def +(other: Option[Milk]): Option[Milk] = {
				ifNotBothEmpty(maybe, other) { (a, b) =>
					Some(Milk(a.maybeValue + b.maybeValue))
				}
			}
		}

		implicit class OptionVanillaOps(maybe: Option[Vanilla]) {
			def maybeValue: Option[Double] = maybe.map(_.Tbsp)

			def +(other: Option[Vanilla]): Option[Vanilla] = {
				ifNotBothEmpty(maybe, other) { (a, b) =>
					Some(Vanilla(a.maybeValue + b.maybeValue))
				}
			}
		}

		implicit class OptionDoubleOps(maybe: Option[Double]) {
			def +(other: Option[Double]): Double = {
				maybe.getOrElse(0.0) + other.getOrElse(0.0)
			}
		}

		implicit class OptionIntOps(maybe: Option[Int]) {
			def +(other: Option[Int]): Int = {
				maybe.getOrElse(0) + other.getOrElse(0)
			}
		}
	}

}