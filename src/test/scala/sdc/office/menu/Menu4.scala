package sdc.office.menu

import sdc.kitchen.Kitchen
import sdc.kitchen.recipe.Ingredient._
import sdc.kitchen.recipe.Recipe

trait Menu4 {

	val recipes: Seq[Recipe] = Seq(
		Kitchen.crackers(Some(Flour(4.5)), Some(Salt(1.0)), Some(Oil(2.0)), Some(Water(1.0)))
	)

}