package sdc.kitchen.recipe.cookbook

import sdc.kitchen.recipe.Ingredient._
import sdc.kitchen.recipe.Recipe

case class Crackers (
  flour: Flour,
  salt: Salt,
  oil: Oil,
  water: Water
) extends Recipe {
	require(flour.cups > 4.25 && flour.cups < 4.75) // 4 1/2 cups flour
	require(salt.tsp   > 0.9  && salt.tsp   < 1.1 ) // 1 teaspoon salt
	require(oil.Tbsp   > 1.9  && oil.Tbsp   < 2.1 ) // 2 Tbsp mild olive oil
	require(water.cups > 0.70 && water.cups < 1.25) // 3/4 - 5/4 cups water

	val name = "Crackers"

	val steps: Seq[String] = Seq(
		"Preheat the oven to 500°F (260°C). Ideally you would place a pizza stone on the bottom oven rack, but realistically a 10-by-15-inch baking sheet will work just dandy.",
		"In a large bowl, mix together all the ingredients, starting with just 3/4 cup water, until everything comes together to form a dough. If the dough seems dry, add a little more water, just a few drops at a time. Be sparing with the water and do not add so much that the dough becomes sticky.",
		"Let the dough rest for 10 to 15 minutes.",
		"Divide the dough into 8 pieces. Flatten a piece slightly and pass it repeatedly through a pasta maker, reducing the thickness each time until you eventually reach the thinnest or minimum setting on your pasta machine. Alternately, you can simply roll the dough as thinly as possible with a rolling pin on a lightly floured surface. Repeat with the remaining dough pieces.",
		"Trim the rolled-out dough pieces into rectangles. (How many pieces of matzoh you get depends on how thinly you rolled the dough.) Use a fork to prick holes in the surface of the dough. lf salted matzoh are desired, brush or spray the dough surface lightly with water and sprinkle with salt to taste.",
		"Carefully place some of the rectangles onto the pizza stone or baking sheet. They should fit snugly but should not touch. Bake until the surface of the matzoh is golden brown and bubbly, 30 to 90 seconds.",
		"Using tongs, carefully flip the matzoh pieces and continue to bake until the other side is golden browned and lightly blistered, 15 to 30 seconds. Keep careful and constant watch to keep the matzoh from burning; the exact baking time will vary from oven to oven and will get longer with subsequent batches. You want to let the matzoh get a few dots of light brown but don’t let the matzoh turn completely brown or it will taste burnt.",
		"Let the matzoh cool before serving. (When our testers made this, they devoured it within hours—and sometimes minutes—of pulling it from the oven, but typically with this sort of baked good you can keep it in an airtight container or resealable plastic bag at room temperature for up to a couple days.)"
	)

	val source = "https://leitesculinaria.com/84910/recipes-homemade-matzoh.html"
}
