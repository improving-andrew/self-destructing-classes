package sdc.office.menu

import sdc.kitchen.Kitchen
import sdc.kitchen.recipe.Ingredient._
import sdc.kitchen.recipe.Recipe

trait Menu1 {

	val recipes: Seq[Recipe] = Seq(
		Kitchen.cake(Some(Butter(2)), Some(Flour(3.0)), Some(BakingPowder(1.0)), Some(Salt(0.5)), Some(Sugar(1.25)), Some(Eggs(4)), Some(Milk(1.25)), Some(Vanilla(1.0)))
	)

}