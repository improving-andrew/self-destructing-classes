package sdc.kitchen.recipe.cookbook

import sdc.kitchen.recipe.Ingredient._
import sdc.kitchen.recipe.Recipe

case class Cake (
  butter: Butter,
  flour: Flour,
  bakingPowder: BakingPowder,
  salt: Salt,
  sugar: Sugar,
  eggs: Eggs,
  milk: Milk,
  vanilla: Vanilla
) extends Recipe {
	require(butter.sticks    == 2                              ) // 2 sticks butter
	require(flour.cups        > 2.9 && flour.cups        < 3.1 ) // 3 cups flour
	require(bakingPowder.Tbsp > 0.9 && bakingPowder.Tbsp < 1.1 ) // 1 Tbsp
	require(salt.tsp          > 0.4 && salt.tsp          < 0.6 ) // 1/2 teaspoon salt
	require(sugar.cups        > 1.2 && sugar.cups        < 1.3 ) // 1 1/4 cups sugar
	require(eggs.count       == 4                              ) // 4 large eggs
	require(milk.cups         > 1.2 && milk.cups         < 1.3 ) // 1 1/4 cups whole milk
	require(vanilla.Tbsp      > 0.9 && vanilla.Tbsp      < 1.1 ) // 1 Tbsp vanilla extract

	val name = "Cake"

	val steps: Seq[String] = Seq(
		"Preheat the oven to 350 degrees F. Butter two 9-inch-round cake pans and line the bottoms with parchment paper; butter the parchment and dust the pans with flour, tapping out the excess.",
		"Whisk 3 cups flour, the baking powder and salt in a bowl until combined. Beat 2 sticks butter and the sugar in a large bowl with a mixer on medium-high speed until light and fluffy, about 3 minutes. Reduce the mixer speed to medium; beat in the eggs, one at a time, scraping down the bowl as needed. Beat in the vanilla. (The mixture may look separated at this point.) Beat in the flour mixture in 3 batches, alternating with the milk, beginning and ending with flour, until just smooth.",
		"Divide the batter between the prepared pans. Bake until the cakes are lightly golden on top and a toothpick inserted into the middle comes out clean, 30 to 35 minutes. Transfer to racks and let cool 10 minutes, then run a knife around the edge of the pans and turn the cakes out onto the racks to cool completely. Remove the parchment. Trim the tops of the cakes with a long serrated knife to make them level, if desired."
	)

	val source = "https://www.foodnetwork.com/recipes/food-network-kitchen/basic-vanilla-cake-recipe-2043654"
}
