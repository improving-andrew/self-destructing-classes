package sdc.office.inventory

import org.scalatest.flatspec.AnyFlatSpecLike
import sdc.kitchen.recipe.Ingredient._
import sdc.office.OfficeTestHelper.assertRequiredInventory

class Menu2 extends sdc.office.menu.Menu2 with AnyFlatSpecLike {

	behavior of "Menu2 Inventory"
	
	it should "generate an inventory update" in {
		assertRequiredInventory(
			items = recipes
		)(
			flour = Some(Flour(4.6)),
			water = Some(Water(1.95)),
			salt = Some(Salt(1.33)),
			oil = Some(Oil(4.0)),
			sugar = Some(Sugar(0.41)),
			vanilla = Some(Vanilla(1.0)),
			yeast = Some(Yeast(1.44)),
			bakingPowder = Some(BakingPowder(1.0)),
			butter = Some(Butter(2)),
			eggs = Some(Eggs(4)),
			milk = Some(Milk(1.25))
		)
	}

}