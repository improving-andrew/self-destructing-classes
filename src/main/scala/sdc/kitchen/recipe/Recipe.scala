package sdc.kitchen.recipe

trait Recipe {
	def name: String
	def steps: Seq[String]
	def source: String
}
