package sdc.office.inventory

import org.scalatest.flatspec.AnyFlatSpecLike
import sdc.kitchen.recipe.Ingredient._
import sdc.office.OfficeTestHelper.assertRequiredInventory

class Menu1 extends sdc.office.menu.Menu1 with AnyFlatSpecLike {

	behavior of "Menu1 Inventory"
	
	it should "generate an inventory update" in {
		assertRequiredInventory(
			items = recipes
		)(
			flour = Some(Flour(3.0)),
			salt = Some(Salt(0.5)),
			sugar = Some(Sugar(1.25)),
			vanilla = Some(Vanilla(1.0)),
			bakingPowder = Some(BakingPowder(1.0)),
			butter = Some(Butter(2)),
			eggs = Some(Eggs(4)),
			milk = Some(Milk(1.25))
		)
	}

}