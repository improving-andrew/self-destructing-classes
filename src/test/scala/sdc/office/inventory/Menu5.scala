package sdc.office.inventory

import org.scalatest.flatspec.AnyFlatSpecLike
import sdc.kitchen.recipe.Ingredient._
import sdc.office.OfficeTestHelper.assertRequiredInventory

class Menu5 extends sdc.office.menu.Menu5 with AnyFlatSpecLike {

	behavior of "Menu5 Inventory"
	
	it should "generate an inventory update" in {
		assertRequiredInventory(
			items = recipes
		)(
			flour = Some(Flour(cups = 13.0)),
			salt = Some(Salt(tsp = 3.0)),
			sugar = Some(Sugar(cups = 1.75)),
			vanilla = Some(Vanilla(1.0)),
			bakingPowder = Some(BakingPowder(1.0)),
			butter = Some(Butter(2)),
			eggs = Some(Eggs(4)),
			milk = Some(Milk(1.25))
		)
	}

}