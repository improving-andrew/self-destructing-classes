package sdc.office

import org.scalatest.Assertion
import org.scalatest.matchers.should.Matchers._
import sdc.kitchen.recipe.Ingredient._
import sdc.kitchen.recipe.Recipe

object OfficeTestHelper {

	def assertRequiredInventory(
		items: Seq[Recipe]
	)(
		flour: Option[Flour] = None,
		water: Option[Water] = None,
		salt: Option[Salt] = None,
		oil: Option[Oil] = None,
		sugar: Option[Sugar] = None,
		vanilla: Option[Vanilla] = None,
		yeast: Option[Yeast] = None,
		bakingPowder: Option[BakingPowder] = None,
		butter: Option[Butter] = None,
		eggs: Option[Eggs] = None,
		milk: Option[Milk] = None
	): Assertion = {

		val office = new Office()
		val actual = office.requiredInventory(items)

		val expected = Inventory(
			yeast = yeast,
			water = water,
			flour = flour,
			oil = oil,
			salt = salt,
			sugar = sugar,
			butter = butter,
			bakingPowder = bakingPowder,
			eggs = eggs,
			milk = milk,
			vanilla = vanilla
		)

		actual.yeast.foreach(a => expected.yeast.foreach(e => a shouldEqual e))
		actual.water.foreach(a => expected.water.foreach(e => a shouldEqual e))
		actual.flour.foreach(a => expected.flour.foreach(e => a shouldEqual e))
		actual.oil.foreach(a => expected.oil.foreach(e => a shouldEqual e))
		actual.salt.foreach(a => expected.salt.foreach(e => a shouldEqual e))
		actual.sugar.foreach(a => expected.sugar.foreach(e => a shouldEqual e))
		actual.butter.foreach(a => expected.butter.foreach(e => a shouldEqual e))
		actual.bakingPowder.foreach(a => expected.bakingPowder.foreach(e => a shouldEqual e))
		actual.eggs.foreach(a => expected.eggs.foreach(e => a shouldEqual e))
		actual.milk.foreach(a => expected.milk.foreach(e => a shouldEqual e))
		actual.vanilla.foreach(a => expected.vanilla.foreach(e => a shouldEqual e))

		succeed
	}

	def assertInvoiceGenerated(
		items: Seq[Recipe]
	)(
		flour: Option[Flour] = None,
		water: Option[Water] = None,
		salt: Option[Salt] = None,
		oil: Option[Oil] = None,
		sugar: Option[Sugar] = None,
		vanilla: Option[Vanilla] = None,
		yeast: Option[Yeast] = None,
		bakingPowder: Option[BakingPowder] = None,
		butter: Option[Butter] = None,
		eggs: Option[Eggs] = None,
		milk: Option[Milk] = None
	): Assertion = {

		val office = new Office()
		val actual = office.generateInvoice(items)

		val expected = Invoice(
			yeast = yeast,
			water = water,
			flour = flour,
			oil = oil,
			salt = salt,
			sugar = sugar,
			butter = butter,
			bakingPowder = bakingPowder,
			eggs = eggs,
			milk = milk,
			vanilla = vanilla
		)

		actual.yeast.foreach(a => expected.yeast.foreach(e => a shouldEqual e))
		actual.water.foreach(a => expected.water.foreach(e => a shouldEqual e))
		actual.flour.foreach(a => expected.flour.foreach(e => a shouldEqual e))
		actual.oil.foreach(a => expected.oil.foreach(e => a shouldEqual e))
		actual.salt.foreach(a => expected.salt.foreach(e => a shouldEqual e))
		actual.sugar.foreach(a => expected.sugar.foreach(e => a shouldEqual e))
		actual.butter.foreach(a => expected.butter.foreach(e => a shouldEqual e))
		actual.bakingPowder.foreach(a => expected.bakingPowder.foreach(e => a shouldEqual e))
		actual.eggs.foreach(a => expected.eggs.foreach(e => a shouldEqual e))
		actual.milk.foreach(a => expected.milk.foreach(e => a shouldEqual e))
		actual.vanilla.foreach(a => expected.vanilla.foreach(e => a shouldEqual e))

		succeed
	}

}
