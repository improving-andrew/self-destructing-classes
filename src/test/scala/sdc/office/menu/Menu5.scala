package sdc.office.menu

import sdc.kitchen.Kitchen
import sdc.kitchen.recipe.Ingredient._
import sdc.kitchen.recipe.Recipe

trait Menu5 {

	val recipes: Seq[Recipe] = Seq(
		Kitchen.cake(Some(Butter(2)), Some(Flour(3.0)), Some(BakingPowder(1.0)), Some(Salt(0.5)), Some(Sugar(1.25)), Some(Eggs(4)), Some(Milk(1.25)), Some(Vanilla(1.0))),
		Kitchen.bread(
			yeast = Some(Yeast(Tbsp = 1.5)),
			water = Some(Water(cups = 2)),
			flour = Some(Flour(cups = 5.5)),
			oil = Some(Oil(4.0)),
			salt = Some(Salt(1.5)),
			sugar = Some(Sugar(0.5))
		),
		Kitchen.crackers(Some(Flour(4.5)), Some(Salt(1.0)), Some(Oil(2.0)), Some(Water(1.0)))
	)

}