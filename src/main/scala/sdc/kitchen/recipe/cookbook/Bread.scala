package sdc.kitchen.recipe.cookbook

import sdc.kitchen.recipe.Ingredient._
import sdc.kitchen.recipe.Recipe

case class Bread (
  yeast: Yeast,
  water: Water,
  flour: Flour,
  oil: Oil,
  salt: Salt,
  sugar: Sugar
) extends Recipe {
	require(yeast.Tbsp > 1.4  && yeast.Tbsp < 1.6 ) // 1 1/2 tablespoons yeast
	require(water.cups > 1.9  && water.cups < 2.1 ) // 2 cups water
	require(flour.cups > 4.5  && flour.cups < 6.5 ) // 5 - 6 cups flour
	require(oil.Tbsp   > 3.5  && oil.Tbsp   < 4.5 ) // 1/4 cup == 4 Tbsp veg oil
	require(salt.tsp   > 1.25 && salt.tsp   < 1.75) // 1 1/2 teaspoons salt
	require(sugar.cups > 0.4  && sugar.cups < 0.6)  // 1/2 cup sugar

	val name = "Bread"

	val steps: Seq[String] = Seq(
		"In a large bowl, dissolve the sugar in warm water and then stir in yeast. Allow to proof until yeast resembles a creamy foam, about 5 minutes.",
		"Mix salt and oil into the yeast. Mix in flour one cup at a time.",
		"Knead dough for 7 minutes. Place in a well oiled bowl, and turn dough to coat. Cover with a damp cloth. Allow to rise until doubled in bulk, about 1 hour.",
		"Punch dough down. Knead for 1 minute and divide in half. Shape into loaves and place into two greased 9×5 inch loaf pans. Allow to rise for 30 minutes, or until dough has risen 1 inch above pans.",
		"Bake at 350 degrees F (175 degrees C) for 30-40 minutes.",
		"Cool, brush with butter and enjoy!"
	)

	val source = "https://butterwithasideofbread.com/homemade-bread/"
}