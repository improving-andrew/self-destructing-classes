# self-destructing-classes

A minimal example of (ab)using the [Uniform Access Principle](https://en.wikipedia.org/wiki/Uniform_access_principle) in Scala to perform side-effecting behavior.

In this case, that behavior rewrites the source code of any file where a field is accessed, replacing that field access with its underlying value.

Once all code has been run where any field accesses occur, all accesses will have been removed, and the data class can manually be removed as well. Hence the repo name "self-destructing-classes".

---

Based on this snippet

https://gitlab.com/-/snippets/2468198

which was used as part of this MR

https://gitlab.com/cpc-tracktrace/domain/service-standard-date/-/merge_requests/848
