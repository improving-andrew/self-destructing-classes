package sdc.kitchen

import sdc.kitchen.recipe.Ingredient._
import sdc.kitchen.recipe.cookbook.{Bread, Cake, Crackers}

/** The `Kitchen` enables us to make `Recipe`s from `Ingredient`s. */
object Kitchen {

	def bread(
		yeast: Option[Yeast],
		water: Option[Water],
		flour: Option[Flour],
		oil: Option[Oil],
		salt: Option[Salt],
		sugar: Option[Sugar],
	): Bread = {
		Bread(
			yeast.getOrElse(Yeast(0)),
			water.getOrElse(Water(0)),
			flour.getOrElse(Flour(0)),
			oil.getOrElse(Oil(0)),
			salt.getOrElse(Salt(0)),
			sugar.getOrElse(Sugar(0))
		)
	}

	def cake(
		butter: Option[Butter],
		flour: Option[Flour],
		bakingPowder: Option[BakingPowder],
		salt: Option[Salt],
		sugar: Option[Sugar],
		eggs: Option[Eggs],
		milk: Option[Milk],
		vanilla: Option[Vanilla]
	): Cake = {
		Cake(
			butter.getOrElse(Butter(0)),
			flour.getOrElse(Flour(0)),
			bakingPowder.getOrElse(BakingPowder(0)),
			salt.getOrElse(Salt(0)),
			sugar.getOrElse(Sugar(0)),
			eggs.getOrElse(Eggs(0)),
			milk.getOrElse(Milk(0)),
			vanilla.getOrElse(Vanilla(0))
		)
	}

	def crackers(
		flour: Option[Flour],
		salt: Option[Salt],
		oil: Option[Oil],
		water: Option[Water]
	): Crackers = {
		Crackers(
			flour.getOrElse(Flour(0)),
			salt.getOrElse(Salt(0)),
			oil.getOrElse(Oil(0)),
			water.getOrElse(Water(0))
		)
	}
}
