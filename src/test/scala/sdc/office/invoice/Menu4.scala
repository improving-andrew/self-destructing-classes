package sdc.office.invoice

import org.scalatest.flatspec.AnyFlatSpecLike
import sdc.kitchen.recipe.Ingredient._
import sdc.office.OfficeTestHelper.assertInvoiceGenerated

class Menu4 extends sdc.office.menu.Menu4 with AnyFlatSpecLike {

	behavior of "Menu4 Invoice"

	it should "generate an invoice" in {
		assertInvoiceGenerated(
			items = recipes
		)(
			flour = Some(Flour(4.5)),
			salt = Some(Salt(1.0)),
			sugar = Some(Sugar(1.25)),
			vanilla = Some(Vanilla(1.0)),
			bakingPowder = Some(BakingPowder(1.0)),
			butter = Some(Butter(2)),
			eggs = Some(Eggs(4)),
			milk = Some(Milk(1.25))
		)
	}

}