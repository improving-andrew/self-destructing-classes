package sdc.office

import sdc.kitchen.recipe.Recipe
import sdc.kitchen.recipe.cookbook.{Bread, Cake, Crackers}

class Office {

	case class UnknownRecipeException(message: String) extends Exception(message)

	def generateInvoice(items: Seq[Recipe]): Invoice = {
		items.map {
			case Bread(yeast, water, flour, oil, salt, sugar) =>
				Invoice(
					yeast = Some(yeast),
					water = Some(water),
					flour = Some(flour),
					oil = Some(oil),
					salt = Some(salt),
					sugar = Some(sugar)
				)

			case Cake(butter, flour, bakingPowder, salt, sugar, eggs, milk, vanilla) =>
				Invoice(
					butter = Some(butter),
					flour = Some(flour),
					bakingPowder = Some(bakingPowder),
					salt = Some(salt),
					sugar = Some(sugar),
					eggs = Some(eggs),
					milk = Some(milk),
					vanilla = Some(vanilla)
				)

			case Crackers(flour, salt, oil, water) =>
				Invoice(
					flour = Some(flour),
					salt = Some(salt),
					oil = Some(oil),
					water = Some(water)
				)

			case unknown => throw UnknownRecipeException(unknown.name)

		}.reduce(_.add(_))
	}

	def requiredInventory(items: Seq[Recipe]): Inventory = {
		items.map {
			case Bread(yeast, water, flour, oil, salt, sugar) =>
				Inventory(
					yeast = Some(yeast),
					water = Some(water),
					flour = Some(flour),
					oil = Some(oil),
					salt = Some(salt),
					sugar = Some(sugar)
				)

			case Cake(butter, flour, bakingPowder, salt, sugar, eggs, milk, vanilla) =>
				Inventory(
					butter = Some(butter),
					flour = Some(flour),
					bakingPowder = Some(bakingPowder),
					salt = Some(salt),
					sugar = Some(sugar),
					eggs = Some(eggs),
					milk = Some(milk),
					vanilla = Some(vanilla)
				)

			case Crackers(flour, salt, oil, water) =>
				Inventory(
					flour = Some(flour),
					salt = Some(salt),
					oil = Some(oil),
					water = Some(water)
				)

			case unknown => throw UnknownRecipeException(unknown.name)

		}.reduce(_.add(_))
	}

}
