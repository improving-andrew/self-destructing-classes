package sdc.office

import sdc.kitchen.recipe.Ingredient.Implicits._
import sdc.kitchen.recipe.Ingredient._

case class Invoice (
	yeast:        Option[Yeast]        = None,
	water:        Option[Water]        = None,
	flour:        Option[Flour]        = None,
	oil:          Option[Oil]          = None,
	salt:         Option[Salt]         = None,
	sugar:        Option[Sugar]        = None,
	butter:       Option[Butter]       = None,
	bakingPowder: Option[BakingPowder] = None,
	eggs:         Option[Eggs]         = None,
	milk:         Option[Milk]         = None,
	vanilla:      Option[Vanilla]      = None
) {

	def add(other: Invoice): Invoice = {
		Invoice(
			yeast        = yeast + other.yeast,
			water        = water + other.water,
			flour        = flour + other.flour,
			oil          = oil + other.oil,
			salt         = salt + other.salt,
			sugar        = sugar + other.sugar,
			butter       = butter + other.butter,
			bakingPowder = bakingPowder + other.bakingPowder,
			eggs         = eggs + other.eggs,
			milk         = milk + other.milk,
			vanilla      = vanilla + other.vanilla
		)
	}
}